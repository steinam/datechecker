using System;


namespace TestDateChecker
{
	/// <summary>
	/// Die Hilfsklase Datum steht f�r die Testf�lle zur Verf�gung
	/// </summary>
	public class Datum
	{

		/// <summary>
		/// Tag
		/// </summary>
		public int day;
		
		/// <summary>
		/// Monat
		/// </summary>
		public int month;
		
		/// <summary>
		/// Jahr
		/// </summary>
		public int year;

		/// <summary>
		/// 
		/// </summary>
		/// <param name="_year"></param>
		/// <param name="_month"></param>
		/// <param name="_day"></param>
		public Datum(int _year, int _month, int _day)
		{
			this.day = _day;
			this.month = _month;
			this.year = _year;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public int getWeekday()
		{
			DateTime d = new DateTime(year, month, day);
			return (int) d.DayOfWeek;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public int getDayOfYear()
		{
			DateTime d = new DateTime(year, month, day);
			return (int) d.DayOfYear;
		}

		/// <summary>
		/// Gibt die Differenz in Tagen zum �bergebenen Datum zur�ck
		/// </summary>
		/// <param name="otherDate"></param>
		/// <returns></returns>
		public int getDiffInDays(Datum otherDate)
		{
			DateTime me = new DateTime(this.year, this.month, this.day);
			DateTime otherDateTime = new DateTime(otherDate.year, otherDate.month, otherDate.day);
			
			TimeSpan diff = me - otherDateTime;
			return diff.Days;
		}
	}
}
