﻿using System;
using System.Text;

namespace DateChecker
{
    class Program
    {
        static void Main(string[] args)
        {

            //int result = KalendarRoutines.Calendar.week_day(1, 1, 2017);







        }

        public int Get_complexity(string SourceCode)
        {
            int result = 0;
            try
            {
                StringBuilder sb = new StringBuilder();
                sb.Append(SourceCode);

                char[] delimiterChars = { ' ', '.', '{', '}', '(', ')', ';' };

                string SourceCodeText = sb.ToString();

                string[] words = SourceCodeText.Split(delimiterChars, StringSplitOptions.RemoveEmptyEntries);

                int No_if = FindMatchesInStringArray("if", words);
                int No_elseIf = FindMatchesInStringArray("elseIf", words);
                int No_while = FindMatchesInStringArray("while", words);
                int No_for = FindMatchesInStringArray("for", words);
                int No_foreach = FindMatchesInStringArray("foreach", words);
                int No_case = FindMatchesInStringArray("case", words);

                int No_default = FindMatchesInStringArray("default", words);
                int No_finaly = FindMatchesInStringArray("finaly", words);
                int No_continue = FindMatchesInStringArray("continue", words);
                int No_continues = FindMatchesInStringArray("continues", words);

                int No_try = FindMatchesInStringArray("try", words);
                int No_catch = FindMatchesInStringArray("catch", words);
                int No_and_op = FindMatchesInStringArray("&", words);
                int No_or_op = FindMatchesInStringArray("||", words);
                int No_words = words.Length;

                result = No_if + No_elseIf + No_while + No_for + No_foreach + No_case + No_default + No_finaly + No_continue + No_continues + No_try + No_catch + No_and_op + No_or_op;
            }
            catch { throw; }
            return result;
        }

        public int FindMatchesInStringArray(string markup, string[] strArray)
        {
            int result = 0;
            try
            {
                for (int i = 0; i < strArray.Length; i++)
                {
                    if (markup.ToLower() == strArray[i].ToLower())
                    {
                        result += 1;
                    }
                }
            }
            catch
            {
                throw;
            }
            return result;
        }
    }
}
